/* Replacement trackball mount for MNT Reform
 * Accepts  chrome steel ball bearing balls
 *
 * Lower mounting shell
 *********/
 
bearing_dia = 2.5;      // Diameter of chrome steel ball bearings
trackball_dia = 25;     // 25mm POM ball (See ASSEMBLY.md in reform)
cup_dia = 27;

// Oversize hole to allow for 3d printer tollerances
bearing_fit_dia = bearing_dia + 0.25;

// Screws are all M2
screw_hole_tapping_dia = 1.6;   // Tapping size for M2 screw
screw_hole_clearance_dia = 2.2; // Clearance hole for M2
heatset_insert_dia = 3.4;       // Outside diameter of heatset insert (Tappex 300113472)

// Sensor opening at bottom
sensor_hole_width =  7.8;
sensor_hole_height = 3.86;

// Outer edge of top flange
flange_width = 42;
flange_height = 20 + 2*6;
flange_depth = 4;
// Cutouts on left and right of flange
flange_cutout_width = (flange_width - 32)/2;
flange_cutout_height = 20;

sensor_mount_dia = 17;
sensor_mount_thickness = 5;
sensor_mount_offset = 15.5; // Bottom to top surfaces
sensor_mount_slot_depth = 1.5;
sensor_mount_slot_height = 5;
sensor_mount_lip_depth = 1;
sensor_mount_hole_spacing = 12;
// Thickness of cup/shell around ball
cup_thickness = 4.2;

mount_screw_to_edge=3;
lid_screw_to_edge=4; 
lid_screws_spacing=24;

// Offset of bearings down from center of trackball
bearing_offset = 5; // 21.5 degree contact angle
bearing_rotation = 15;

threaded_inserts = false;   // Size top mounting holes for threaded inserts?
sphere_fn=64;
cylinder_fn=64;
/******************************************************************************/

// The origin is the center of the trackball, all other coords based around this

//trackball();                // Only show trackball for modelling, don't print
cup();


module trackball() 
{
    sphere(d = trackball_dia, $fn=sphere_fn);
}

module cup()
{
    cup_depth = (trackball_dia / 2) + cup_thickness;
    difference() {
        union() {
            /* Flange */
            translate( [0, 0, - (flange_depth/2)] )
                cube( [flange_width, flange_height, flange_depth], center=true );
            /* Ball cup */
            difference() {
                s_dia = (cup_dia+cup_thickness);
                sphere(d = s_dia, $fn=sphere_fn);
                translate([0,0,s_dia/2])
                    cube( [s_dia,s_dia,s_dia], center=true);
            }
            /* Sensor mount */
            translate([0,0,-(sensor_mount_offset-sensor_mount_thickness/2)])
                cylinder(h = sensor_mount_thickness, d = sensor_mount_dia, $fn=cylinder_fn, center=true);
            /* Sensor mount lip */
            translate([0,0,-(sensor_mount_offset+sensor_mount_lip_depth/2)]) 
                difference() {
                    lip_height = (sensor_mount_dia/2 - sensor_mount_slot_height) -sensor_hole_height/2;
                    cylinder(h = sensor_mount_lip_depth, d = sensor_mount_dia, $fn=cylinder_fn, center=true);
                    translate([0,-lip_height/2,0])
                        cube([sensor_mount_dia,sensor_mount_dia - lip_height,sensor_mount_lip_depth], center=true);
                }
            
        }
        /* Cutouts */
        /* Ball void/cup */
        sphere(d = cup_dia, $fn=sphere_fn);
        /* Side flange cutouts */
        translate( [-((flange_width/2) - flange_cutout_width/2),0,-flange_depth/2] )
            cube( [flange_cutout_width, flange_cutout_height, flange_depth], center=true);
        translate( [+((flange_width/2) - flange_cutout_width/2),0,-flange_depth/2] )
            cube( [flange_cutout_width, flange_cutout_height, flange_depth], center=true);
        /* Sensor opening */
        translate( [0,0,0] )
            cube( [sensor_hole_width, sensor_hole_height, 100], center=true);
        /* Sensor mount slot */
        translate( [0,+(sensor_hole_height/2 + sensor_mount_slot_height/2),-(sensor_mount_offset-sensor_mount_slot_depth/2)] )
            cube( [sensor_mount_dia, sensor_mount_slot_height, sensor_mount_slot_depth], center=true);
        /* Sensor mounting holes */
        translate( [-sensor_mount_hole_spacing/2,0,0] )
            cylinder(h=100, d=screw_hole_tapping_dia, $fn=cylinder_fn, center=true);
        translate( [sensor_mount_hole_spacing/2,0,0] )
            cylinder(h=100, d=screw_hole_tapping_dia, $fn=cylinder_fn, center=true);
        /* Upper mounting holes */
        translate([-(flange_width/2-mount_screw_to_edge),+(flange_height/2-mount_screw_to_edge),-flange_depth/2])
            cylinder(h=flange_depth, d=screw_hole_tapping_dia, $fn=cylinder_fn, center=true);
        translate([+(flange_width/2-mount_screw_to_edge),+(flange_height/2-mount_screw_to_edge),-flange_depth/2])
            cylinder(h=flange_depth, d=screw_hole_tapping_dia, $fn=cylinder_fn, center=true);
        translate([-(flange_width/2-mount_screw_to_edge),-(flange_height/2-mount_screw_to_edge),-flange_depth/2])
            cylinder(h=flange_depth, d=screw_hole_tapping_dia, $fn=cylinder_fn, center=true);
        translate([+(flange_width/2-mount_screw_to_edge),-(flange_height/2-mount_screw_to_edge),-flange_depth/2])
            cylinder(h=flange_depth, d=screw_hole_tapping_dia, $fn=cylinder_fn, center=true);
        /* Lid mounting holes */
        lid_holes_dia = (threaded_inserts?heatset_insert_dia:screw_hole_tapping_dia);
        
        translate([-lid_screws_spacing/2,-(flange_height/2-lid_screw_to_edge),0])
            cylinder(h=100, d=lid_holes_dia, $fn=cylinder_fn, center=true);
        translate([lid_screws_spacing/2,-(flange_height/2-lid_screw_to_edge),0])
            cylinder(h=100, d=lid_holes_dia, $fn=cylinder_fn, center=true);
        /* Bearing mounts */        
        translate([0,0,-bearing_offset])
        rotate([0,0,bearing_rotation])
        bearing_mounts(bearing_offset);
            
    }
}








/* Positive of bearing mounts 
 * Spheres in equilateral triangle, with specified distance to center
 */
module bearing_mounts(bearing_offset)
{
    // Radius of circle intersecting sphere at offset from center.
    function circ_of_sphere_radius(s_r, off) = sqrt(s_r*s_r - off*off);

    // Circle of sphere at center offset of bearing (not contact point)
    c_of_s_bearing = circ_of_sphere_radius(trackball_dia/2, bearing_offset);
    // Offset of the point that would touch the ball
    // Y component of line from center of bearing to origin minus radius at angle5
    bearing_contact_angle = atan( bearing_offset / (c_of_s_bearing+(bearing_dia/2)));

    echo("Bearing contact angle5: ", bearing_contact_angle);

    // Offset down sphere of contact point
    //contact_offset = sin(bearing_contact_angle) * (trackball_dia/2);

    dc = cos(bearing_contact_angle) * (trackball_dia/2 + bearing_dia/2);

    
    union(){
        translate([0,dc,0])
            sphere(d=bearing_fit_dia, $fn=sphere_fn);
        translate([cos(210)*dc,sin(210)*dc,0])
        sphere(d=bearing_fit_dia, $fn=sphere_fn);
        translate([cos(330)*dc,sin(330)*dc,0])
        sphere(d=bearing_fit_dia, $fn=sphere_fn);
    }
}


